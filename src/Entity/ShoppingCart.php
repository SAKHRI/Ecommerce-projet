<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 */
class ShoppingCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $total;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductLine", mappedBy="shoppingCart", orphanRemoval=true)
     */
    private $product_line;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="shoppingCart", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->product_line = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return Collection|ProductLine[]
     */
    public function getProductLine(): Collection
    {
        return $this->product_line;
    }

    public function addProductLine(ProductLine $productLine): self
    {
        if (!$this->product_line->contains($productLine)) {
            $this->product_line[] = $productLine;
            $productLine->setShoppingCart($this);
        }

        return $this;
    }

    public function removeProductLine(ProductLine $productLine): self
    {
        if ($this->product_line->contains($productLine)) {
            $this->product_line->removeElement($productLine);
            // set the owning side to null (unless already changed)
            if ($productLine->getShoppingCart() === $this) {
                $productLine->setShoppingCart(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
